# Stoma guide

This project is about creating smart, durable and easy to use guides for helping with the action of cutting stomabags to a perfect fit for your illistomi (and others in future).

## Autodesk Fusion 360

At the moment we are using Autodesks services for storing the cad files. In future this might change..

[Autodesk project and files](https://a360.co/40AX4xl)

## Naming convention

|Purpose|Model name|Version|
|-------|----------|-------|
|sg     |cc        |0      |


## Models

### stomaguide-creditcard-0

Modelname: sg-cc-0

This is the first guide we have created.

The idéa was to create a guide that could fit inside a wallet.

