## ISO/IEC 7810 Identification cards

The standard defines four card sizes:
- ID-1
- ID-2
- ID-3
- ID-000

### Characteristcs

- Material properties
	- Resistance to bending
	- Flames
	- Chemicals
	- Temeprature
	- Humidity
	- Toxicity

### Physical Dimensions

#### ISO/IEC 7810#ID-1

Most banking cards and ID cards.

|Part   |Dimension    |
|-------|-------------|
|Length |85,60 mm     |
|Width  |53,98 mm     |
|Height |0,76 mm      |
|Corner |2,88-3,48 mm |

#### ISO/IEC 7810#ID-2

French and other ID cards (Visas)

|Part   |Dimension    |
|-------|-------------|
|Length |105,0 mm     |
|Width  |74,0 mm      |
|Height |0,76 mm      |
|Corner |2,88-3,48 mm |

#### ISO/IEC 7810#ID-3

Passports.

|Part   |Dimension    |
|Length |125,0 mm     |
|-------|-------------|
|Width  |88,0 mm      |
|Height |0,76 mm      |
|Corner |2,88-3,48 mm |

#### ISO/IEC 7810#ID-000

SIM.

|Part   |Dimension    |
|-------|-------------|
|Length |25,0 mm      |
|Width  |15,0 mm      |
|Height |0,76 mm      |
|Corner |2,88-3,48 mm |
