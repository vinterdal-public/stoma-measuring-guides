# Braille

Reference:
- https://www.brailleauthority.org/size-and-spacing-braille-characters

## Dimensions

|Measurement Range                                               |Minimum/Maximum                     |
|----------------------------------------------------------------|------------------------------------|
|Dot Base Diameter                                               |1,5mm to 1,6mm<br>0,059" to 0.063"  |
|Distance between two dots in the same cell                      |2,3mm to 2,5mm<br>0,090" to 0.100"  |
|Distance between corresponding dots in adjacent cells           |6,1mm to 7,6mm<br>0,241" to 0.300"  |
|Dot height                                                      |0,6mm to 0,9mm<br>0,025" to 0.037"  |
|Distance between corresponding dots from one cell directly below|10,0mm to 10,2mm<br>0,395" to 0.400"|

